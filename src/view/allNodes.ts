import { ViewArg } from "./View";

export function allNodes({ extensionPacksByExtension }: ViewArg): string {
	return `
	// all nodes
	${extensionPacksByExtension
			.map(([extension]) => extension)
			.join('\n\t')
		}
`
}
