import { Extension, ExtensionId } from "./Extension"
import { PathLike, promises } from "fs"

const { readdir, readFile } = promises

export type ExtensionsById = Map<ExtensionId, Extension>

export async function getExtensionsById(
	extensionsFolder: PathLike
): Promise<ExtensionsById> {
	const extensionsById = new Map<ExtensionId, Extension>()

	for await (const extensionFolder of await readdir(extensionsFolder)) {
		const packageJson = `${extensionsFolder}/${extensionFolder}/package.json`

		try {
			const fileContent = await readFile(packageJson)

			const {
				name,
				displayName = name,
				extensionDependencies,
				extensionPack
			} = JSON.parse(fileContent.toString())

			const publisher = extensionFolder.replace(/\..*$/, "")

			const extension = new Extension({
				publisher,
				name,
				displayName,
				extensionDependencies,
				extensionPack
			})

			extensionsById.set(extension.id, extension)
		} catch (ex) {
			console.error(extensionFolder, ex.message)
		}
	}

	return extensionsById
}
