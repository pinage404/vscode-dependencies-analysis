export type ExtensionId = string

function toId(name: string): ExtensionId {
	return name.toLowerCase()
}

export class Extension {
	id: ExtensionId
	publishName: string
	publisher: string
	name: string
	displayName: string
	extensionDependencies: ExtensionId[]
	extensionPack: ExtensionId[]

	constructor({
		publisher,
		name,
		displayName = name,
		extensionDependencies = [],
		extensionPack = []
	}: {
		publisher: string
		name: string
		displayName: string
		extensionDependencies: string[]
		extensionPack: string[]
	}) {
		const publishName = `${publisher}.${name}`

		this.id = toId(publishName)
		this.publishName = publishName
		this.publisher = publisher
		this.name = name
		this.displayName = displayName
		this.extensionDependencies = extensionDependencies.map(toId)
		this.extensionPack = extensionPack.map(toId)
	}

	dependsOn(extension: Extension): string {
		return `"${this.publishName}" -> "${extension.publishName}"`
	}

	toString() {
		return `"${this.publishName}" [label="${this.displayName}"]`
	}
}
