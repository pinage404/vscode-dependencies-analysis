import { ExtensionPacksByExtension } from "../getExtensionPacksByExtension";
import { ExtensionPacksIdById } from "../getExtensionPacksIdById";

export type ViewArg = {
    extensionPacksByExtension: ExtensionPacksByExtension,
    extensionPacksIdById: ExtensionPacksIdById,
}

export type View = (arg: ViewArg) => string
