import { ExtensionPacksByExtension } from "./getExtensionPacksByExtension"
import { ExtensionPacksIdById } from "./getExtensionPacksIdById"
import { View } from "./view/View"

export function generateGraph({
	extensionPacksByExtension,
	extensionPacksIdById,
	sections
}: {
	extensionPacksByExtension: ExtensionPacksByExtension
	extensionPacksIdById: ExtensionPacksIdById
	sections: View[]
}) {
	return (
		'digraph "Dependencies" {' +
		sections
			.map(section =>
				section({ extensionPacksByExtension, extensionPacksIdById })
			)
			.join("\n") +
		"}"
	)
}
