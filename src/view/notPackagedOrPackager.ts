import { ViewArg } from "./View";
import { clusterExtensions } from "./utils/clusterExtensions";

export function notPackagedOrPackager({ extensionPacksByExtension, extensionPacksIdById }: ViewArg): string {
	return clusterExtensions(
		'Not packaged or packager',
		extensionPacksByExtension.filter(([extension, extensionsPacks]) =>
			extensionsPacks.length === 0 &&
			!Array.from(extensionPacksIdById.values()).some(extensionPacksId =>
				extensionPacksId.includes(extension.id)
			)
		)
	)
}
