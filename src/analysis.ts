#!/usr/bin/env node

import { allNodes } from "./view/allNodes"
import { generateGraph } from "./generateGraph"
import { getExtensionPacksByExtension } from "./getExtensionPacksByExtension"
import { getExtensionPacksIdById } from "./getExtensionPacksIdById"
import { getExtensionsById } from "./getExtensionsById"
import { links } from "./view/links"
import { manualInstalled } from "./view/manualInstalled"
import { notPackagedOrPackager } from "./view/notPackagedOrPackager"
import { oncePackaged } from "./view/oncePackaged"
import { severalTimesPackaged } from "./view/severalTimesPackaged"

const EXTENSIONS_FOLDER = ".vscode/extensions"

// Choose how do you want to group the extensions
const sections = [
	severalTimesPackaged,
	oncePackaged,
	manualInstalled,
	notPackagedOrPackager,
	allNodes,
	links,
]

	;
(async () => {
	const extensionsById = await getExtensionsById(EXTENSIONS_FOLDER)

	const extensionPacksIdById = getExtensionPacksIdById(extensionsById)

	const extensionPacksByExtension = getExtensionPacksByExtension({
		extensionsById,
		extensionPacksIdById
	})

	const graph = generateGraph({
		extensionPacksByExtension,
		extensionPacksIdById,
		sections,
	})

	console.log(graph)
})()
