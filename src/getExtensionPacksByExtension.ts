import { Extension } from "./Extension"
import { ExtensionPacksIdById } from "./getExtensionPacksIdById"
import { ExtensionsById } from "./getExtensionsById"

export type ExtensionPacksByExtension = [Extension, Extension[]][]

export function getExtensionPacksByExtension({
	extensionPacksIdById,
	extensionsById
}: {
	extensionPacksIdById: ExtensionPacksIdById
	extensionsById: ExtensionsById
}): ExtensionPacksByExtension {
	return Array.from(extensionPacksIdById.entries()).map(
		([id, extensionPacksId]): [Extension, Extension[]] => [
			extensionsById.get(id)!,
			extensionPacksId.map(
				extensionPackId => extensionsById.get(extensionPackId)!
			)
		]
	)
}
