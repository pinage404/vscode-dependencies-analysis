import { ViewArg } from "./View";
import { clusterExtensions } from "./utils/clusterExtensions";

export function oncePackaged({ extensionPacksByExtension }: ViewArg): string {
	return clusterExtensions(
		'Once packaged',
		extensionPacksByExtension.filter(([, extensionsPacks]) =>
			extensionsPacks.length === 1
		)
	)
}
