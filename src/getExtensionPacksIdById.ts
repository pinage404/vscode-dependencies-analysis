import { ExtensionId } from "./Extension"
import { ExtensionsById } from "./getExtensionsById"

export type ExtensionPacksIdById = Map<ExtensionId, ExtensionId[]>

export function getExtensionPacksIdById(
	extensionsById: ExtensionsById
): ExtensionPacksIdById {
	const extensionPacksIdById = new Map<ExtensionId, ExtensionId[]>()

	for (const id of extensionsById.keys()) {
		extensionPacksIdById.set(id, [])
	}

	for (const [
		extensionPackId,
		{ extensionPack, extensionDependencies }
	] of extensionsById.entries()) {
		for (const id of extensionPack.concat(extensionDependencies)) {
			if (/^vscode\./.test(id)) {
				// depencency to internal package
				continue
			}

			if (extensionPacksIdById.has(id)) {
				const extensionPacks = extensionPacksIdById.get(id)!
				extensionPacks.push(extensionPackId)
			} else {
				console.warn(`${id} is not installed but required by ${extensionPackId}`)
			}
		}
	}

	return extensionPacksIdById
}
