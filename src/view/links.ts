import { ViewArg } from "./View";

export function links({ extensionPacksByExtension }: ViewArg): string {
	return `
	// links
	${extensionPacksByExtension
			.map(([extension, extensionsPacks]) =>
				extensionsPacks
					.map(extensionsPack =>
						extensionsPack.dependsOn(extension)
					)
					.join('\n\t')
			)
			.join('\n\t')
		}
`
}
