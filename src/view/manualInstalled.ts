import { ViewArg } from "./View";
import { clusterExtensions } from "./utils/clusterExtensions";

export function manualInstalled({ extensionPacksByExtension }: ViewArg): string {
	return clusterExtensions(
		'Manual installed',
		extensionPacksByExtension.filter(([, extensionsPacks]) =>
			extensionsPacks.length === 0
		)
	)
}
