export function cluster(name: string, content: string[]): string {
	return `
	subgraph "cluster_${name}" {
		label = "${name}"

		${content.join('\n\t\t')}
	}
`
}
