import { ExtensionPacksByExtension } from "../../getExtensionPacksByExtension";
import { cluster } from "./cluster";

export function clusterExtensions(name: string, extensions: ExtensionPacksByExtension): string {
	return cluster(
		name,
		extensions
			.map(([extension]) => extension)
			.map(extension => extension.toString())
	)
}
