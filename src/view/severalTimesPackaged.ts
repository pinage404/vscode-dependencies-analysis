import { ViewArg } from "./View";
import { clusterExtensions } from "./utils/clusterExtensions";

export function severalTimesPackaged({ extensionPacksByExtension }: ViewArg): string {
	return clusterExtensions(
		'Several times packaged',
		extensionPacksByExtension.filter(([, extensionsPacks]) =>
			extensionsPacks.length > 1
		)
	)
}
