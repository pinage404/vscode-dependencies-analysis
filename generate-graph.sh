#!/bin/sh
cd $(realpath $(dirname $0))

# @TODO: use NVM or upgrade NPM
docker run \
    --rm \
    --interactive \
    --tty \
    --workdir /w \
    --volume $(pwd):/w \
    --volume ~/.vscode/extensions/:/w/.vscode/extensions/:ro \
    --volume ~/Project/vscode/:/w/Project/vscode:ro \
    node:10.11 \
    npm run generate-graph

cd generated

dot \
    -Tsvg \
    -o dependencies-dot.svg \
    dependencies.dot
dot \
    -Tsvg \
    -Kneato \
    -o dependencies-neato.svg \
    dependencies.dot
dot \
    -Tsvg \
    -Kfdp \
    -o dependencies-fdp.svg \
    dependencies.dot
